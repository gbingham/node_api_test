const cliArgs = require('./lib/cliArgs.js');

const config = {
  hostnameTG: 'https://techgenie-svc-dev.vivint.com',
  hostnameSG: 'https://streetgenie-svc-dev.vivint.com',
  superUser: 'gvb',
  salesUser: 'jason.king',
  salesUserId: '30009',
  state: 'UT', // must be uppercase two character
  officeId: '6088',
};

config.hostnameTG = cliArgs.tghost || config.hostnameTG;
config.hostnameSG = cliArgs.tghost || config.hostnameSG;
config.superUser = process.env.SUPERUSER || cliArgs.superuser || config.superUser;
config.salesUser = process.env.SALESUSER || cliArgs.salesuser || config.salesUser;
config.officeId = process.env.OFFICEID || cliArgs.officeid || config.officeId;
config.salesUserId = cliArgs.salesid || config.salesUserId;
config.state = cliArgs.state || config.state;

module.exports = config;
