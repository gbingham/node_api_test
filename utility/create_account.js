const api = require('chakram');
const config = require('../config.js')
const authDefaults = require('../lib/SG/authenticate.js');
const homeDefaults = require('../lib/SG/home.js');
const creditDefaults = require('../lib/SG/runcreditcheck.js');
const createBillingDefaults = require('../lib/SG/createbillingaccount.js');
const requestTechDefaults = require('../lib/SG/requesttech.js');
const packageDefaults = require('../lib/SG/package.js');
const chalk = require('chalk');

const expect = api.expect;

describe('Create Account', function () {
  this.timeout(999999);
  it('should authenticate', () => {
    return api.post(...authDefaults).then((resp) => {
      updateHeadersWith(resp.body.data.SessionToken);
      updateCreditDefaultsWith(resp);
    });
  });
  it('should create a new address and contact', () => {
    return api.post(...homeDefaults).then((resp) => {
      updateCreditDefaultsWith(resp);
      updateRequestTechDefaultsWith(resp);
    });
  });
  it('should run a credit check and create an account', () => {
    return api.post(...creditDefaults).then((resp) => {
      updateCreateBillingDefaultsWith(resp);
      updatePackageDefaultsWith(resp);
    });
  });
  it('should create a billing account', () => {
    return api.post(...createBillingDefaults).then((resp) => {
    });
  });
  it('should save a package', () => {
    return api.post(...packageDefaults).then((resp) => {
    });
  });
  it('should dispatch to tech', () => {
    return api.post(...requestTechDefaults).then((resp) => {
    });
  });
  after(() => {
    outputInfo();
  });
});

function updateHeadersWith(data) {
  homeDefaults[2].headers.SessionToken = data;
  creditDefaults[2].headers.SessionToken = data;
  createBillingDefaults[2].headers.SessionToken = data;
  requestTechDefaults[2].headers.SessionToken = data;
  packageDefaults[2].headers.SessionToken = data;
  // TODO maybe use this to set instead
  // for(var i in require.cache) {console.log(require.cache[i].exports.headers)}
}
function updateCreditDefaultsWith(data) {
  if (typeof data.body.data.User !== 'undefined') {
    creditDefaults[1].salesOfficeID = data.body.data.User.officeID;
  } else {
    creditDefaults[1].addressID = data.body.data.addressID;
    creditDefaults[1].contacts[0].firstName = homeDefaults[1].firstName;
    creditDefaults[1].contacts[0].lastName = homeDefaults[1].lastName;
    creditDefaults[1].contacts[0].phone = homeDefaults[1].phoneNumber;
    creditDefaults[1].contacts[0].residentID = data.body.data.householdMembers[0].residentID;
    // creditDefaults[1].salesOfficeID = flowData.salesOfficeID;
    // creditDefaults[2].headers.SessionToken = flowData.SessionToken;
  }
}
function updateRequestTechDefaultsWith(data) {
  requestTechDefaults[1].AddressID = data.body.data.addressID;
  requestTechDefaults[1].InstallDate = new Date().toISOString();
}
function updateCreateBillingDefaultsWith(data) {
  config.OpportunityID = data.body.data.OpportunityID;
  createBillingDefaults[0] = createBillingDefaults[0].concat(`?opportunityID=${data.body.data.OpportunityID}`);
}
function updatePackageDefaultsWith(data) {
  packageDefaults[0] = packageDefaults[0].concat(`?opportunity=${data.body.data.OpportunityID}&forceUpdate:true`);
}
function outputInfo() {
  let obj = {};
  obj.firstName = homeDefaults[1].firstName;
  obj.lastName = homeDefaults[1].lastName;
  obj.address = homeDefaults[1].address;
  obj.city = homeDefaults[1].city;
  obj.state = homeDefaults[1].state;
  obj.link = `https://vivint--devgrnacre.cs22.my.salesforce.com/${config.OpportunityID}`
  for(let key in obj) {
    console.log(chalk.blue(obj[key]));
  }
}
