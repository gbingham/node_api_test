module.exports = {
  updateHeadersWith(data) {
    homeDefaults[2].headers.SessionToken = data;
    creditDefaults[2].headers.SessionToken = data;
    createBillingDefaults[2].headers.SessionToken = data;
    requestTechDefaults[2].headers.SessionToken = data;
    packageDefaults[2].headers.SessionToken = data;
  // TODO maybe use this to set instead
  // for(var i in require.cache) {console.log(require.cache[i].exports.headers)}
  },
  updateCreditDefaultsWith(data) {
    if (typeof data.body.data.User !== 'undefined') {
      creditDefaults[1].salesOfficeID = data.body.data.User.officeID;
    } else {
      creditDefaults[1].addressID = data.body.data.addressID;
      creditDefaults[1].contacts[0].firstName = homeDefaults[1].firstName;
      creditDefaults[1].contacts[0].lastName = homeDefaults[1].lastName;
      creditDefaults[1].contacts[0].phone = homeDefaults[1].phoneNumber;
      creditDefaults[1].contacts[0].residentID = data.body.data.householdMembers[0].residentID;
    // creditDefaults[1].salesOfficeID = flowData.salesOfficeID;
    // creditDefaults[2].headers.SessionToken = flowData.SessionToken;
    }
  },
  updateRequestTechDefaultsWith(data) {
    requestTechDefaults[1].AddressID = data.body.data.addressID;
    requestTechDefaults[1].InstallDate = new Date().toISOString();
  },
  updateCreateBillingDefaultsWith(data) {
    createBillingDefaults[0] = createBillingDefaults[0].concat(`?opportunityID=${data.body.data.OpportunityID}`);
  },
  updatePackageDefaultsWith(data) {
    packageDefaults[0] = packageDefaults[0].concat(`?opportunity=${data.body.data.OpportunityID}&forceUpdate:true`);
  },
  outputInfo() {
    let obj = {};
    obj.firstName = homeDefaults[1].firstName;
    obj.lastName = homeDefaults[1].lastName;
    obj.address = homeDefaults[1].address;
    obj.city = homeDefaults[1].city;
    obj.state = homeDefaults[1].state;
    for(let key in obj) {
      console.log(chalk.blue(obj[key]));
    }
  }
}
