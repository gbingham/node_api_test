const range = require('./addressPickRange.js');

module.exports = {
  get() {
    const random = Math.random() * (range[1] - range[0]) + range[0];
    return Math.floor(random);
  },
};
