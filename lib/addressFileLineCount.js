const config = require('../config.js');
const exec = require('child_process').execSync;

function getAddressFileLineCount() {
  let resp = exec(`wc -l ./addresses/${config.state}.json`);
  resp = resp.toString();
  resp = resp.trim();
  resp = resp.match(/\d+/, '');
  return resp[0];
}

module.exports = getAddressFileLineCount();
