const addressFileLineCount = require('./addressFileLineCount.js');

function addressRange() {
  const count = addressFileLineCount;
  const upper = count - 1;
  const lower = 2;
  return [lower, upper];
}

module.exports = addressRange();
