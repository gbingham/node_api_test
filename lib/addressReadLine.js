const exec = require('child_process').execSync;
const config = require('../config.js');

module.exports = {
  get(index) {
    let resp = exec(`sed "${index}q;d" ./addresses/${config.state}.json`);
    resp = resp.toString();
    resp = resp.trim();
    resp = resp.replace(/,$/, '');
    resp = JSON.parse(resp);
    return resp;
  },
};
