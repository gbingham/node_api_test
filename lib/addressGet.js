const index = require('./addressRandomIndex.js');
const addressReadLine = require('./addressReadLine.js');

module.exports = {
  get() {
    return addressReadLine.get(index.get());
  },
};
