const cliArgs = require('../../lib/cliArgs.js');
const defaultData = require('../../defaults/SG/authenticate.js');

if (cliArgs.user) {
  defaultData[1].UserName = cliArgs.user;
}
if (cliArgs.pass) {
  defaultData[1].Password = process.env.PASSWORD || cliArgs.pass;
}

module.exports = defaultData;
