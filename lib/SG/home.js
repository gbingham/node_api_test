const chance = require('chance').Chance();
const cliArgs = require('../../lib/cliArgs.js');
const defaultData = require('../../defaults/SG/home.js');
const addressGet = require('../../lib/addressGet.js');

const address = addressGet.get();

defaultData[1].Location.coordinates = [
  address.LAT,
  address.LON,
];

defaultData[1].address = cliArgs.address || `${address.NUMBER} ${address.STREET}`;
defaultData[1].city = cliArgs.city || address.CITY;
defaultData[1].phoneNumber = cliArgs.phone || chance.phone();
defaultData[1].postalCode = cliArgs.zip || address.POSTCODE;
defaultData[1].state = cliArgs.state || address.REGION;
defaultData[1].firstName = cliArgs.first || chance.first();
defaultData[1].lastName = cliArgs.last || chance.last();
// TODO need to do birthday just need the format

module.exports = defaultData;
