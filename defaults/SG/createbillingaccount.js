const config = require('../../config.js');

module.exports = [
  `${config.hostnameSG}/api/Packaging/CreateBillingAccount`,
  {
  },
  {
    headers: {
      'Content-Type': 'application/json',
      SessionToken: '',
    },
    json: true,
  },
];

/*
 REQUEST: POST Packaging / CreateBillingAccount ? opportunityID = 006 P0000008PxhAIAS
 with Parameters: (null)
 RESPONSE: POST / api / Packaging / CreateBillingAccount
 executionTime = 1.750602 sec
 with result: {
    data = 8 a80812b5d175928015d513d4bb40bf0;
    notifications = ({
        errorCode = 0;
        message = success;
        notificationSeverity = success;
    });
    success = 1;
}
*/
