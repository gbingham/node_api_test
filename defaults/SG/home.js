const config = require('../../config.js');

module.exports = [
  `${config.hostnameSG}/api/streettracker/Home`,
  {
    Location: {
      coordinates: (
        '-111.8607127213262',
        '40.3857130611238'
      ),
      type: 'Point',
    },
    accountType: null,
    address: '2178 Revere Way',
    advocateFirstName: null,
    advocateLastName: null,
    advocatePhoneNumber: null,
    city: 'Eagle Mountain',
    dateOfBirth: null,
    firstName: 'SGTEST',
    isReferral: false,
    lastName: 'TESTSG',
    notes: '',
    phoneNumber: '(801) 734-8643',
    postalCode: 84005,
    state: 'UT',
    forceUpdate: true,
  },
  {
    headers: {
      'Content-Type': 'application/json',
      SessionToken: '',
    },
    json: true,
  },
];
