const config = require('../../config.js');

module.exports = [
  `${config.hostnameSG}/api/streettracker/runcreditcheck?forceUpdate:false`,
  {
    addressID: 121798564,
    contacts: [{
      ContactID: null,
      SSN: null,
      customerType: 1,
      dateOfBirth: null,
      email: '',
      firstName: 'Test',
      isHardInquiry: 0,
      lastName: 'Again',
      phone: 8017378698,
      prevCity: null,
      prevState: null,
      prevStreet: null,
      prevZip: null,
      residentID: 346840538,
      runCredit: 1,
      signature: null,
    }],
    repLat: 0,
    repLong: 0,
    salesOfficeID: 8114,
  },
  {
    headers: {
      'Content-Type': 'application/json',
      SessionToken: '',
    },
    json: true,
  },
];
/*
    Jul 17 09: 35: 20 MBP - GBINGHAM Street Genie[1604]: with result: {
      data: {
        AddressID: 121798564;
        CreditGradeText: PASS;
        CreditResidents: ({
          ContactID: 003 P0000019okIiIAI;
          CreditGradeText: PASS;
          CreditScore: P;
          Signature: "<null>";
          customerType: 1;
          dateOfBirth: "";
          firstName: Test;
          hasRunPreviousAddressCheck: 0;
          lastName: Again;
          passedHardCreditCheck: 1;
          passedSoftCreditCheck: 1;
          phone: 8017378698;
          residentID: 346840538;
        });
        CreditScore: P;
        DateCreated: "2017-07-17T15:35:20.7190864Z";
        ErrorMessage: "";
        OpportunityID: 006 P0000008PxhAIAS;
        SprintServiceCoverage: GOOD;
        UserID: 30009;
        accountNumber: "A-9643801";
        addressStatus: {
          addressID: 121798564;
          dateSet: "2017-07-17T15:35:10.4689968Z";
          employeeID: 30009;
          notes: "<null>";
          statusID: 11;
          statusReasonID: 69;
        };
        isQualifiedForSprint: 1;
      };
      notifications: ({
        errorCode: 0;
        message: success;
        notificationSeverity: success;
      });
      success: 1;
    }
    */
