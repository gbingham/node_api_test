const config = require('../../config.js');

module.exports = [
  `${config.hostnameSG}/api/StreetTracker/RequestTech`,
  {
    AddressID: 121795251,
    InstallDate: '2017-08-01T18:45:35Z', // new Date().toISOString();
    invOfficeID: `${config.officeId}`,
  },
  {
    headers: {
      'Content-Type': 'application/json',
      SessionToken: '',
    },
    json: true,
  },
];
/*
Aug  1 12:44:58 MBP-GBINGHAM Street Genie[34111]: REQUEST: POST StreetTracker/RequestTech
Aug  1 12:44:58 MBP-GBINGHAM Street Genie[34111]: with Parameters: 
{
	    AddressID = 121795251;
	    InstallDate = "2017-08-01T18:45:35Z";
	    invOfficeID = 6088;
	}
Aug  1 12:44:59 MBP-GBINGHAM Street Genie[34111]: RESPONSE: POST /qa2/api/StreetTracker/RequestTech
Aug  1 12:44:59 MBP-GBINGHAM Street Genie[34111]:   executionTime = 0.191742 sec
Aug  1 12:44:59 MBP-GBINGHAM Street Genie[34111]: with result: {
	    data =     {
	        addressStatus =         {
	            addressID = 121795251;
	            dateSet = "2017-08-01T18:44:59.9323818Z";
	            employeeID = 30009;
	            notes = "Requested installation date is 8/1/2017 6:45:35 PM";
	            statusID = 12;
	            statusReasonID = 75;
	        };
	        requestedDate = "2017-08-01T18:45:35Z";
	    };
	    notifications =     (
	                {
	            errorCode = 0;
	            message = success;
	            notificationSeverity = success;
	        }
	    );
	    success = 1;
	}
*/
