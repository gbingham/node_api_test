const config = require('../../config.js');

module.exports = [
  `${config.hostnameSG}/api/Authentication/Authenticate`,
  {
    UserName: `${config.superUser}/${config.salesUser}`,
    Password: '',
    DeviceID: 'F2BE24D3-06E3-4D56-873C-5B2F900EFDDB',
    DeviceType: 'iPad',
    OSVersion: '9.3',
    AppVersion: '2.0.135',
    Latitude: 40.42997583425544,
    Longitude: -111.8967055683957,
  },
];
